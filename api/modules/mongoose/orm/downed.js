var mongoose = require('mongoose');
const extendSchema = require('mongoose-extend-schema');
const EventSchema = require('./event');

var DownedSchema = new extendSchema(EventSchema,{
    date: {
        type: Date,
        required: true
    }

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Downed', DownedSchema);



