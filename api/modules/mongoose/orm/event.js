var mongoose = require('mongoose');
var EventSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    description: {
        type: String,
        required: true
    },
    weight:  {
        type: mongoose.Schema.Types.Number,
    },
    editable: {
        type: Boolean,
        default: () => true
    },
    latitude: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    longitude: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    dateTime: {
        type: Date,
        default: () =>  Date.now()
    },
    lotID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'lot'
    },
    userID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    valueChainOperatorID:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ValueChainOperator',
    }
});

module.exports = EventSchema;
