var mongoose = require('mongoose');

var LotSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    lotNumber: {
        type: String,
        required: true,
        unique: true
    },
    weight: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    unit: {
        type: String,
        required: true
    },
    downedWeight: {
        type: mongoose.Schema.Types.Number,
        default: () => 0
    },
    creationDate: {
        type: Date,
        required: true
    },
    expirationDate: {
        type: Date,
        required: false
    },
    productID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product',
        required: true
    }
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

LotSchema.index({lotNumber: 1, valueChainOperatorID: 1}, { unique: true });

module.exports = mongoose.model('Lot', LotSchema);
