var mongoose = require('mongoose');

var OperatorTypeSchema = new mongoose.Schema({
    available: {
        type: Boolean,
        default: true
    },
    valueChainOperatorID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ValueChainOperator',
        unique: false,
        required: true
    },
    typeOfValueChainOperatorID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TypeOfValueChainOperator',
        unique: false,
        required: true
    },
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});


OperatorTypeSchema.index({valueChainOperatorID: 1, typeOfValueChainOperatorID: 1}, {unique: true});

module.exports = mongoose.model('OperatorType', OperatorTypeSchema);
