var mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    name: {
        type: String,
        required: true
    },
    description: String,

    qtdProtein: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    qtdLipids: {
        type: mongoose.Schema.Types.Number
    },
    qtdHydrates: {
        type: mongoose.Schema.Types.Number
    },
    available: {
        type: Boolean,
        required: true,
    },
    productTypeID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ProductType',
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Product', ProductSchema);