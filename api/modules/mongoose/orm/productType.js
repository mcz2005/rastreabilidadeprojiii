var mongoose = require('mongoose');

var ProductTypeSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    name: {
        type: String,
        required: true
    },
    available: {
        type: Boolean,
        required: true
    },

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });


module.exports = mongoose.model('ProductType', ProductTypeSchema);