var mongoose = require('mongoose');

var ProductTypeOfHarvestSchema = new mongoose.Schema({
    available: {
        type: Boolean,
        default: true
    },
    typeOfHarvest: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TypeOfHarvest',
        unique: false,
        required: true
    },
    productType: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ProductType',
        unique: false,
        required: true
    }

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

ProductTypeOfHarvestSchema.index({typeOfHarvest: 1, productType: 1}, { unique: true });

module.exports = mongoose.model('ProductTypeOfHarvest', ProductTypeOfHarvestSchema);

