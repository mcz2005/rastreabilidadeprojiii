var mongoose = require('mongoose');
const extendSchema = require('mongoose-extend-schema');
const EventSchema = require('./event');

var ProductionRegistration = new extendSchema(EventSchema,{
    geographicZone: {
        type: String,
        required: true
    },
    temperature: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    qualityIndicator: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    typeOfHarvestID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TypeOfHarvest',
        required: true
    }

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('ProductionRegistration', ProductionRegistration);