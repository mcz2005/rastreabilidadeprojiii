var mongoose = require('mongoose');
const extendSchema = require('mongoose-extend-schema');
const EventSchema = require('./event');


var QualityAssessment = new extendSchema(EventSchema,{
    qualityIndicator: {
        type: String,
        required: true
    },
    temperature: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    newLotID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'lot'
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('QualityAssessment', QualityAssessment);
