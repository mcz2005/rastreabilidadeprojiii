var mongoose = require('mongoose');
const extendSchema = require('mongoose-extend-schema');
const EventSchema = require('./event');

var SaleSchema = new extendSchema(EventSchema,{
    soldToConsumer: {
        type: Boolean,
        required: true
    },
    salePrice: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    valueChainOperatorDestinyID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ValueChainOperator'
    },
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Sale', SaleSchema);