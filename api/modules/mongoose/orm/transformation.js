var mongoose = require('mongoose');
const extendSchema = require('mongoose-extend-schema');
const EventSchema = require('./event');

var TransformationSchema = new extendSchema(EventSchema, {
    newLotID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'lot'
    }
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

module.exports = mongoose.model('Trasformation', TransformationSchema);

