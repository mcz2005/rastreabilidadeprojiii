var mongoose = require('mongoose');
const extendSchema = require('mongoose-extend-schema');
const EventSchema = require('./event');

var TransportationSchema = new extendSchema(EventSchema,{
    finalDateTime: {
        type: Date,
        required: true
    },
    minTemperature: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    maxTemperature: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    avgTemperature: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    qualityIndicator: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    valueChainOperatorDestinyID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ValueChainOperator'
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Transportation', TransportationSchema);