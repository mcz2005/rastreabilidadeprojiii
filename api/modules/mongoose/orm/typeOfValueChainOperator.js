var mongoose = require('mongoose');

var TypeOfValueChainOperatorSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    typeName: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    available:{
        type: Boolean,
        required: true
    },
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

module.exports = mongoose.model('TypeOfValueChainOperator', TypeOfValueChainOperatorSchema);