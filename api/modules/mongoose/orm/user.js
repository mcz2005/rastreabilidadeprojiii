var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    phone:{
        type: mongoose.Schema.Types.Number,
        unique: true,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    valueChainOperatorID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ValueChainOperator'
    },
    available: {
        type: Boolean,
        required: true
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('User', UserSchema);