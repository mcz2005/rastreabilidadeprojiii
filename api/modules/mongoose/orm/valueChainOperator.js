var mongoose = require('mongoose');

var ValueChainOperatorSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    name: {
        type: String,
        required: true
    },
    abbreviation:{
        type: String,
        required: true,
        unique: true
    },
    available: {
        type: Boolean,
        required: true
    },
    onlySellsToConsumer: {
        type: Boolean,
        required: true
    },
    address: {
        type: String,
        required: false
    },
    latitude: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    longitude: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    postalCodeID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PostalCode'
    }
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

module.exports = mongoose.model('ValueChainOperator', ValueChainOperatorSchema);