let JwtStrategy = require('passport-jwt').Strategy;
let ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../../mongoose/orm/user');

//Middleware para verificar se é recebido um token e se este é valido ou não. Primeiro configura-se as opções para
//ir buscar o token ao header, declara-se a chave secreta (usada para desencriptar o token e por ultimo atribui-se
//ao passport a estratégia usada para verificação do token.
//jwt_payload --> Informação do token desencriptado
//Se na informação do token houver o id de um user (e token valido) adiciona o utilizador ao pedido e passa para o controller
module.exports = function (passport) {

    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = process.env.JWT_SECRET;

    passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
        User.findById(jwt_payload.id, function(err, user) {
            if (err) {
                return done(err, false);
            }
            if (user) {
                return  done(null, user);
            } else {
                return done(null, false);
            }
        });
    }));
};
