require('dotenv').config();
const express                       = require('express');
const app                           = express();
const bodyParser                    = require('body-parser');
const router		                = require('express-promise-router')();
let mongoose                        = require('mongoose');
const swaggerUi                     = require('swagger-ui-express');
const swaggerDocument               = require('./swagger/swagger.json');
const cors                          = require('cors')

// a aplicação express vai usar o 'body-parser' para fazer o get dos dados recebidos a partir do post
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// Selecionar a porta para o servidor correr, neste caso deixamos a porta 3000 default
const port = process.env.PORT || 3001;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://mongodb:27017/rastreabilidade', { useNewUrlParser: true })
    .then(() => console.log('connection successful'))
    .catch((err) => console.error(err));

app.use(function (req, res, next) {
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type');
    res.setHeader('Access-Control-Expose-Headers','Authorization');

    next();
});

// Define a prefix for all routes
// Can define something unique like MyRestAPI
// We'll just leave it so all routes are relative to '/'

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(require('../routers/index')(router, app));



// Start server listening on port 3000
app.listen(port);
console.log('RESTAPI listening on port: ' + port);
