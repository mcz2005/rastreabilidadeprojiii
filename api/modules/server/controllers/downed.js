//const express = require('express');
//const router = express.Router();
const Lot = require('../../mongoose/orm/lot');
const Downed = require('../../mongoose/orm/downed');
const EventLog = require('../../mongoose/orm/eventLog');
const Event = require('../controllers/event');

module.exports = {

//Method to get all the entries from the table "Downed" from the database
    getAll: function (req, res) {
        Downed.find(function (err, downs) {
            if (downs && downs.length > 0) {
                res.status(process.env.SUCCESS).json(downs)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No downs inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to search an entrie on the table "Downed" by ID
    getById: function (req, res) {
        Downed.findById(req.params.id, function (err, downs) {
            if (downs) {
                res.status(process.env.SUCCESS).json(downs)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Downed not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to post a entrie on the table "Downed"
    create: function (req, res) {
        req.body['valueChainOperatorID'] = req.user.valueChainOperatorID;
        req.body['userID'] = req.user._id
        let eventLog = {};
        eventLog['userId'] = req.user._id;
        eventLog["eventName"] = "Downed";
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (lot && lot.length > 0) {
                req.body['lotID'] = lot[0]._id;
                if (lot[0].downedWeight && (lot[0].weight - lot[0].downedWeight) >= req.body.weight || !lot[0].downedWeight) {
                    Downed.create(req.body, async function (err, downed) {
                        await Event.updateEditableForAllEvents(downed.lotID, downed._id);
                        if (err) {
                            return res.status(err.status || process.env.ERROR).json({
                                'message': err || 'Some error occurred.'
                            });
                        }
                        lot[0].downedWeight += downed.weight;
                        Lot.findByIdAndUpdate(lot[0]._id, lot[0], function (err, lot) {
                            if (err) return res.status(err.status || process.env.ERROR).json({
                                'message': err || 'Some error occurred.'
                            });
                            eventLog["message"] = "Error when try create Downed Event";
                            EventLog.create(eventLog, function (err, eventLog) {
                            })
                            if (lot) {
                                eventLog["eventId"] = downed._id;
                                eventLog["message"] = "Event Downed save successfully"
                                EventLog.create(eventLog, function (err, eventLog) {
                                    res.json(downed);
                                })
                            }
                        });
                    });
                } else {
                    res.json({'message': 'Not enough quantity on the lot'});
                }
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to edit a entrie on the table "Downed" by ID
    update: function (req, res) {
        let alreadyDowned;
        Downed.findById(req.params.id, function (err, downed) {
            alreadyDowned = downed.weight;
        })
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
                if (lot && lot.length > 0) {
                    req.body['lotID'] = lot[0]._id;
                    console.log((lot[0].weight - (lot[0].downedWeight - alreadyDowned)) >= req.body.weight)
                    if (lot[0].downedWeight && (lot[0].weight - (lot[0].downedWeight - alreadyDowned)) >= req.body.weight || !lot[0].downedWeight) {
                        Downed.findByIdAndUpdate(req.params.id, req.body, function (err, downed) {
                            console.log(req.body)
                            if (err) res.status(err.status || process.env.ERROR).json({
                                'message': err || 'Some error occurred.'
                            });
                            lot[0].downedWeight += (downed.weight - alreadyDowned);
                            Lot.findByIdAndUpdate(lot[0]._id, lot[0], function (err, lot) {
                                if (err) res.status(err.status || process.env.ERROR).json({
                                    'message': err || 'Some error occurred.'
                                });
                                if (lot) {
                                    res.status(process.env.SUCCESS).json(downed);

                                }
                            });
                        });
                    } else {
                        res.status(process.env.ERROR).json({'message': 'Not enough quantity on the lot'});
                    }
                }
            }
        ).catch(err => {
            return res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to delete a entrie on the table "Downed" by ID
    delete:

        function (req, res) {
            Downed.findByIdAndRemove(req.params.id, req.body, function (err, downed) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(downed);
            });
        }

    ,
    getByValueChainOperator: function (req, res) {
        Downed.find({valueChainOperatorID: req.user.valueChainOperatorID}, function (err, downs) {
            if (downs && downs.length > 0) {
                res.status(process.env.SUCCESS).json(downs)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No downs inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    }
    ,
}
;
