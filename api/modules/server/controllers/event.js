//const express = require('express');
//const router = express.Router();

const ProductionResgistration = require('../../mongoose/orm/productionRegistration');
const Storing = require('../../mongoose/orm/storing');
const Sale = require('../../mongoose/orm/sale');
const Transformation = require('../../mongoose/orm/transformation');
const Transportation = require('../../mongoose/orm/transportation');
const Downed = require('../../mongoose/orm/downed');
const QualityAssessment = require('../../mongoose/orm/qualityAssessment');
const Lot = require('../../mongoose/orm/lot');

const eventTypes = [Storing, Sale, Downed, Transportation, ProductionResgistration, Transformation, QualityAssessment];
const eventTypesToSearchEvents = [Storing, Sale,Downed, Transportation, ProductionResgistration, Transformation, QualityAssessment];

const getAllEventsForLotId = async function (lotId, traceability) {
    let evento;
    for (let eventType of eventTypesToSearchEvents) {
        if (eventType === QualityAssessment || eventType === Transformation) {
            let event = await eventType.find({newLotID: lotId});
            if (event) {
                for (let e of event) {
                    let traceabilityContaisEvent = await verifyIfTraceabilityAlreadyContainsEvent(traceability, e);
                    if (!traceabilityContaisEvent) {
                        evento = JSON.parse(JSON.stringify(e));
                        evento['eventType'] = eventType.modelName;
                        traceability.push(evento);
                        await getAllEventsForLotId(evento.lotID, traceability);
                    }
                }
            }
        }
        if (eventType !== Transformation) {
            event = await eventType.find({lotID: lotId});
            if (event) for (let e of event) {
                let traceabilityContaisEvent = await verifyIfTraceabilityAlreadyContainsEvent(traceability, e);
                if (!traceabilityContaisEvent) {
                    evento = JSON.parse(JSON.stringify(e));
                    evento['eventType'] = eventType.modelName;
                    traceability.push(evento);
                }
            }
        }
    }
    return traceability;
}
const verifyIfTraceabilityAlreadyContainsEvent = async function (traceability, evento) {
    for (let trace of traceability) {
        if (trace._id == evento._id) {
            return true;
        }
    }
    return false;
}
module.exports = {


    getTraceability: function (req, res) {
        let traceability = [];
        Lot.find({lotNumber: req.params.lotNumber}).then(async lot => {
            if (lot.length > 0) {
                traceability = await getAllEventsForLotId(lot[0]._id, traceability);
                traceability.sort((a, b) => {
                    return a.dateTime > b.dateTime ? 1 : -1
                });
            }
            res.send(traceability);
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },
    async updateEditableForAllEvents(lotId, excludeID) {
        for (let eventType of eventTypes) {
            if (eventType === QualityAssessment || eventType === Transformation) {
                await eventType.find({newLotID: lotId}, function (err, event) {
                    for (let e of event) {
                        if (e.editable && e._id.toString() != excludeID.toString()) {
                            e.editable = !e.editable;
                            eventType.findByIdAndUpdate(e._id, e, function (err, evt) {
                            });
                        }
                    }
                }).catch(err => {
                    res.status(err.status || process.env.ERROR).json({
                        'message': err.message || 'Some error occurred.'
                    })
                });
            }
            await eventType.find({lotID: lotId}, function (err, event) {
                for (let e of event) {
                    if (e.editable && e._id.toString() != excludeID.toString()) {
                        e.editable = !e.editable;
                        eventType.findByIdAndUpdate(e._id, e, function (err, evt) {
                        });
                    }
                }
            }).catch(err => {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                })
            });
        }
    }
}
;