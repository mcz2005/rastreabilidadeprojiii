const EventLog = require('../../mongoose/orm/eventLog');

module.exports = {

    getbyUser: function (req, res) {
        EventLog.find({userId: req.user._id}, function (err, eventLog) {
            if (eventLog && eventLog.length > 0) {
                res.status(process.env.SUCCESS).json(eventLog)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No eventLog inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },
}
