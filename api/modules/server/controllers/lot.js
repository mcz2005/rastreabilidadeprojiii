//const express = require('express');
//const router = express.Router();
const Lot = require('../../mongoose/orm/lot');

module.exports = {

//Method to get all the entries from the table "Lot" from the database
    getAll: function (req, res) {
        Lot.find(function (err, lots) {
            if (lots && lots.length > 0) {
                res.status(process.env.SUCCESS).json(lots)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Lots inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

    //Method to search an entrie on the table "Lot" by ID
    getById: function (req, res) {
        Lot.findById(req.params.id, function (err, lots) {
            if (lots) {
                res.status(process.env.SUCCESS).json(lots)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Lot not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

    getByLotNumber: function (req, res) {
        Lot.find({
            valueChainOperatorID: req.body.valueChainOperatorID,
            lotNumber: req.body.lotNumber
        }, function (err, lots) {
            if (lots) {
                res.status(process.env.SUCCESS).json(lots)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Lot not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },


    //Method to post a entrie on the table "Lot"
    create: function (req, res) {
        Lot.create(req.body, function (err, lot) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(lot);
        });
    },

    //Method to edit a entrie on the table "Lot" by ID
    update: function (req, res) {
        Lot.findByIdAndUpdate(req.params.id, req.body, function (err, lot) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json({
                'message': lot
            });
        });
    },

    //Method to delete a entrie on the table "Lot" by ID
    delete: function (req, res) {
        Lot.findByIdAndRemove(req.params.id, req.body, function (err, lot) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(lot);
        });
    },

    isLotWeightCorrect(lot, eventWeight) {
        return ((lot.weight - lot.downedWeight) >= eventWeight) ? true : false;
    }

};
