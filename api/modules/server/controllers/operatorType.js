//const express = require('express');
//const router = express.Router();
const OperatorType = require('../../mongoose/orm/operatorType');

module.exports = {

//Method to get all the entries from the table "OperatorType" from the database
    getAll: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            OperatorType.find(function (err, operatorType) {
                if (operatorType && operatorType.length > 0) {
                    res.status(process.env.SUCCESS).json(operatorType)
                }
                else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No operator Type inserted yet.'})
                }
            }).catch(err => {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                })
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to get all the available entries from the table "OperatorType" from the database
    getAvailable: function (req, res) {
        OperatorType.find({available: true}, function (err, operatorTypes) {
            if (operatorTypes && operatorTypes.length > 0) {
                res.status(process.env.SUCCESS).json(operatorTypes);
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Availabe Operator types insert yet'});
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({'message': err.message || 'Some error occurred'});
        })
    },
//Method to search an entrie on the table "OperatorType" by ID
    getById: function (req, res) {
        OperatorType.findById(req.params.id, function (err, operatorType) {
            if (operatorType) {
                res.status(process.env.SUCCESS).json(operatorType)
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'Type not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },
//Method to search an entrie on the table "OperatorType" by ID
    getByValueChainOperatorID: function (req, res) {
        OperatorType.find({valueChainOperatorID: req.params.valuechainOperatorid, available: true}, function (err, operatorType) {
            if (operatorType) {
                res.status(process.env.SUCCESS).json(operatorType)
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'Type not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to post a entrie on the table "OperatorType"
    create: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            OperatorType.create(req.body, function (err, operatorType) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(operatorType);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to edit a entrie on the table "OperatorType" by ID
    update: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            OperatorType.findByIdAndUpdate(req.params.id, req.body, function (err, operatorType) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(operatorType);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to delete a entrie on the table "OperatorType" by ID
    updateAvailability: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            OperatorType.findById(req.params.id, function (err, operatorType) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                operatorType.available = !operatorType.available;
                operatorType.save(function (err) {
                    if (err) {
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    res.status(process.env.SUCCESS).json(operatorType);
                });
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },


    //Method to delete a entrie on the table "User" by ID
    delete: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            OperatorType.findById(req.params.id, function (err, operatorType) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                operatorType.available = !operatorType.available;
                operatorType.save(function (err) {
                    if (err) {
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    res.status(process.env.SUCCESS).json(operatorType);
                });
            });
        } else {
            return res.status(process.env.NOT_FOUND).json('Unauthorized');
        }
    },



};