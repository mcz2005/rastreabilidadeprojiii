const Product = require('../../mongoose/orm/product');

module.exports = {

//Method to get all the entries (available or unavailable) from the table "Products" from the database
    getAll: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            Product.find(function (err, products) {
                if (products && products.length > 0) {
                    res.status(process.env.SUCCESS).json(products);
                }
                else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Products inserted yet.'});
                }
            }).catch(function (err) {
                return res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                });
            });
        } else {
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
        }
    },

//Method to get all the available entries from the table "Products" from the database
    getAvailable: function (req, res) {
            Product.find({available: true}, function (err, products) {
                if (products && products.length > 0) {
                    res.status(process.env.SUCCESS).json(products);
                }
                else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Available Products inserted'})
                }
            }).catch(function (err) {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                });
            });
    },


//Method to search an entrie on the table "Products" by ID
    getById: function (req, res) {
        Product.findById(req.params.id, function (err, products) {
            if (products) {
                res.status(process.env.SUCCESS).json(products);
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'Product not found.'});
            }
        }).catch(function (err) {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            });
        });
    },

//Method to post a entrie on the table "Products"
    create: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            Product.create(req.body, function (err, product) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                res.status(process.env.SUCCESS).json(product);
            });
        } else {
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
        }
    },

//Method to edit a entrie on the table "Products" by ID
    update: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            Product.findByIdAndUpdate(req.params.id, req.body, function (err, product) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                res.status(process.env.SUCCESS).json(product);
            });
        } else {
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
        }
    },

//Method to delete a entrie on the table "Products" by ID
    updateAvailability: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            Product.findById(req.params.id, function (err, product) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                product.available = !product.available;

                product.save(function (err) {
                    if (err) {
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    res.status(process.env.SUCCESS).json(product);
                });
            });
        } else {
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
        }
    }

};

