const ProductType = require('../../mongoose/orm/productType');
const ProductTypeOfHarvest = require('../../mongoose/orm/productTypeOfHarvest')

module.exports = {

//Method to get all the entries from the table "ProductType" from the database
    getAll: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            ProductType.find(function (err, producttype) {
                if (producttype && producttype.length > 0) {
                    res.status(process.env.SUCCESS).json(producttype);
                }
                else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Product Types inserted yet.'});
                }
            }).catch(err => {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                })
            })
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

    //Method to get all the available entries from the table "ProductType" from the database
    getAvailable: function (req, res) {
        ProductType.find({available: true}, function (err, prodcutType) {
            if (prodcutType && prodcutType.length > 0) {
                res.status(process.env.SUCCESS).json(prodcutType);
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Availabe Product Types inserted'});
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred'
            });
        })
    },

    //Method to search an entrie on the table "ProductType" by ID
    getById: function (req, res) {
        ProductType.findById(req.params.productTypeID, function (err, producttype) {
            if (producttype) {
                res.status(process.env.SUCCESS).json(producttype);
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'Product Type not found.'});
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            });
        });
    },


    //Method to post a entrie on the table "ProductType"
    create: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            let typesOfHarvest = req.body.types;
            delete req.body.typesOfHarvests;
            ProductType.create(req.body, function (err, producttype) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                typesOfHarvest.forEach(typeOhHarvevst =>{
                    ProductTypeOfHarvest.create(
                        {
                            typeOfHarvest: typeOhHarvevst._id,
                            productType: producttype._id
                        }, typeofHarvest =>{
                            console.log("Ok")
                        })
                })

                res.status(process.env.SUCCESS).json(producttype);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

    //Method to edit a entrie on the table "ProductType" by ID
    update: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            let typesOfHarvestEdited = req.body.types;
            delete req.body.types;
            ProductType.findByIdAndUpdate(req.params.id, req.body, function (err, productType) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                ProductTypeOfHarvest.find({productType: req.params.id}, function (err, producttypeofharvest) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });

                    typesOfHarvestEdited.forEach(typeEdited =>{
                        let exists = false;
                        producttypeofharvest.forEach(oldType =>{
                            if(typeEdited._id == oldType.typeOfHarvest) {
                                if(!oldType.available){
                                    oldType.available=true;
                                    ProductTypeOfHarvest.findOneAndUpdate({_id: oldType._id},oldType, function (err, producttypeofharvest) {
                                        if (err) return res.status(err.status || process.env.ERROR).json({
                                            'message': err || 'Some error occurred.'
                                        });
                                    })
                                }
                                exists = true;
                            }
                        });

                        if(!exists){
                            ProductTypeOfHarvest.create(
                                {
                                    typeOfHarvest: typeEdited._id,
                                    productType: productType._id
                                }, producttypeofharvestt=>{
                                })
                        }
                    });

                    producttypeofharvest.forEach(oldType =>{
                        let exists = false;
                        typesOfHarvestEdited.forEach(typeEdited => {
                            if(typeEdited._id == oldType.typeOfHarvest) {
                                exists = true;
                            }
                        });
                        if(!exists) {
                            if(oldType.available) {
                                oldType.available = false;
                                ProductTypeOfHarvest.findOneAndUpdate({_id: oldType._id}, oldType, function (err, producttypeofharvest) {
                                    if (err) return res.status(err.status || process.env.ERROR).json({
                                        'message': err || 'Some error occurred.'
                                    });
                                });
                            }
                        }
                    })

                    res.status(process.env.SUCCESS).json(producttypeofharvest);

                });
            });

        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

    //Method to delete a entrie on the table "ProductType" by ID
    updateAvailability: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            ProductType.findById(req.params.id, function (err, productType) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });

                productType.available = !productType.available;
                productType.save(function (err) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    res.status(process.env.SUCCESS).json(productType);
                });
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    }
};