//const express = require('express');
//const router = express.Router();
const ProductTypeOfHarvest = require('../../mongoose/orm/productTypeOfHarvest');

module.exports = {

//Method to get all the entries from the table "ProductTypeOfHarvest" from the database
    getAll: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            ProductTypeOfHarvest.find(function (err, productTypeOfHarvest) {
                if (productTypeOfHarvest && productTypeOfHarvest.length > 0) {
                    res.status(process.env.SUCCESS).json(productTypeOfHarvest)
                }
                else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Product Types of Harvest inserted yet.'})
                }
            }).catch(err => {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                })
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to get all the available entries from the table "ProductTypeOfHarvest" from the database
    getAvailable: function (req, res) {
        ProductTypeOfHarvest.find({available: true}, function (err, productTypeOfHarvest) {
            if (productTypeOfHarvest && productTypeOfHarvest.length > 0) {
                res.status(process.env.SUCCESS).json(productTypeOfHarvest);
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Availabe Product Harvest Types inserted'});
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred'
            });
        })
    },
//Method to search an entrie on the table "ProductTypeOfHarvest" by ID
    getById: function (req, res) {
        ProductTypeOfHarvest.findById(req.params.id, function (err, productTypeOfHarvest) {
            if (productTypeOfHarvest) {
                res.status(process.env.SUCCESS).json(productTypeOfHarvest)
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'Type not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },


    getByProductTypeID: function(req, res){
        ProductTypeOfHarvest.find({productType: req.params.producttypeid, available: true}, function (err, producttypeofharvest) {
         if(producttypeofharvest){
             res.status(process.env.SUCCESS).json(producttypeofharvest)
         }
         else{
             res.status(process.env.NOT_FOUND).json({'message': 'Type not found'})
         }
        }).catch(err =>{
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        })
    },


//Method to post a entrie on the table "ProductTypeOfHarvest"
    create: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            ProductTypeOfHarvest.create(req.body, function (err, productTypeOfHarvest) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(productTypeOfHarvest);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to edit a entrie on the table "ProductTypeOfHarvest" by ID
    update: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            ProductTypeOfHarvest.findByIdAndUpdate(req.params.id, req.body, function (err, productTypeOfHarvest) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(productTypeOfHarvest);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to delete a entrie on the table "ProductTypeOfHarvest" by ID
    updateAvailability: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            ProductTypeOfHarvest.findById(req.params.id, function (err, productTypeOfHarvest) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                productTypeOfHarvest.available = !productTypeOfHarvest.available;
                productTypeOfHarvest.save(function (err) {
                    if (err) {
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    res.status(process.env.SUCCESS).json(productTypeOfHarvest);
                });
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    }

};