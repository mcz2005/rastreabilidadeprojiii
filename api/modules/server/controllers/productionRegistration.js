//const express = require('express');
//const router = express.Router();
const ProductionRegistration = require('../../mongoose/orm/productionRegistration');
const ValueChainOperator = require('../../mongoose/orm/valueChainOperator')
const Lot = require('../../mongoose/orm/lot');
const EventLog = require('../../mongoose/orm/eventLog');

module.exports = {

//Method to get all the entries from the table "ProductionRegistration" from the database
    getAll: function (req, res) {
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (lot) {
                req.body['lotID'] = lot[0]._id;
                ProductionRegistration.find(function (err, productionRegistration) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    if (productionRegistration && productionRegistration.length > 0) {
                        res.status(process.env.SUCCESS).json(productionRegistration)
                    } else {
                        res.status(process.env.NOT_FOUND).json({'message': 'No Production/Harvest inserted yet.'})
                    }
                });
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to search an entrie on the table "ProductionRegistration" by ID
    getById: function (req, res) {
        ProductionRegistration.findById(req.params.id, function (err, productionRegistrations) {
            if (productionRegistrations) {
                res.status(process.env.SUCCESS).json(productionRegistrations)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Production/Harvest not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to post a entrie on the table "ProductionRegistration"
    create: function (req, res) {
        console.log('teste');
        let productionRegistration = {};
        let lot = {}
        lot['productID'] = req.body.productId;
        lot['unit'] = req.body.unit;
        lot['weight'] = req.body.lotWeight;
        lot['creationDate'] = req.body.lotCreationDate;
        lot['expirationDate'] = req.body.expirationDate;
        lot['valueChainOperatorID'] = req.user.valueChainOperatorID;
        let eventLog = {};
        eventLog['userId'] = req.user._id;
        eventLog["eventName"] = "Production Registration";
        ValueChainOperator.findById(req.user.valueChainOperatorID, function (err, valuechainoperator) {
            if (err) {
                console.log(err)
                eventLog["message"] = "Error when try find the Value Chain";
                EventLog.create(eventLog, function (err, eventLog) {
                })
                return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
            }
            if (valuechainoperator) {
                lot['lotNumber'] = valuechainoperator.abbreviation + '-' + req.body.lotNumber;
                productionRegistration['longitude'] = req.body.longitude;
                productionRegistration['latitude'] = req.body.latitude;
                productionRegistration['geographicZone'] = req.body.geographicZone;
                productionRegistration['description'] = req.body.description;
                productionRegistration['weight'] = req.body.weight;
                productionRegistration['typeOfHarvestID'] = req.body.typeOfHarvestId;
                productionRegistration['qualityIndicator'] = req.body.qualityIndicator;
                productionRegistration['temperature'] = req.body.temperature;
                productionRegistration['userID'] = req.user._id;
                productionRegistration['valueChainOperatorID'] = req.user.valueChainOperatorID;
                Lot.create(lot, function (err, lot) {
                    if (err) {
                        console.log(err)
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                        eventLog["message"] = "Error when try  create the lot for the event Production Registration";
                        EventLog.create(eventLog, function (err, eventLog) {
                        })
                    } else {
                        console.log(lot);
                        productionRegistration['lotID'] = lot._id;
                        ProductionRegistration.create(productionRegistration, function (err, productionRegistration) {
                            if (err) {
                                console.log(err)
                                eventLog["message"] = "Error when try  create the lot for the event Production Registration";
                                EventLog.create(eventLog, function (err, eventLog) {
                                });
                                return res.status(err.status || process.env.ERROR).json({
                                    'message': err || 'Some error occurred.'
                                });
                            }
                            eventLog["eventId"] = productionRegistration._id;
                            eventLog["message"] = "Event Production Registration save successfully";
                            EventLog.create(eventLog, function (err, eventLog) {
                                console.log(productionRegistration)
                                res.status(process.env.SUCCESS).json(productionRegistration);
                                console.log(eventLog);
                            })
                        })
                    }
                });
            }
        });
    },

//Method to edit a entrie on the table "ProductionRegistration" by ID
    update: function (req, res) {
        let productionRegistration = {};
        let lot = {}
        lot['productID'] = req.body.productId;
        lot['unit'] = req.body.unit;
        lot['weight'] = req.body.lotWeight;
        lot['creationDate'] = req.body.lotCreationDate;
        lot['expirationDate'] = req.body.expirationDate;
        lot['lotID'] = req.body.lotID;
        ProductionRegistration.findById(req.params.id, function (err, productionRegist) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            if (productionRegist.editable != null && productionRegist.editable) {
                productionRegistration['longitude'] = req.body.longitude;
                productionRegistration['latitude'] = req.body.latitude;
                productionRegistration['geographicZone'] = req.body.geographicZone;
                productionRegistration['description'] = req.body.description;
                productionRegistration['weight'] = req.body.weight;
                productionRegistration['typeOfHarvestID'] = req.body.typeOfHarvestID;
                productionRegistration['qualityIndicator'] = req.body.qualityIndicator;
                productionRegistration['temperature'] = req.body.temperature;
                productionRegistration['_id'] = req.body._id;
                Lot.findByIdAndUpdate(lot.lotID, lot, function (err, lot) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    else {
                        ProductionRegistration.findByIdAndUpdate(productionRegistration._id, productionRegistration, function (err, productionRegistration) {
                            if (err) return res.status(err.status || process.env.ERROR).json({
                                'message': err || 'Some error occurred.'
                            });
                            res.status(process.env.SUCCESS).json(productionRegistration);
                        })
                    }
                });
            }
        });
    },

//Method to delete a entrie on the table "ProductionRegistration" by ID
    delete: function (req, res) {
        ProductionRegistration.findById(req.params.id, function (err, productionRegist) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            if (productionRegist.editable != null && productionRegist.editable) {
                ProductionRegistration.findByIdAndRemove(req.params.id, req.body, function (err, productionRegistration) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    res.status(process.env.SUCCESS).json(productionRegistration);
                });
            }
        })
    },

    getByValueChainOperatorId: function (req, res) {
        ProductionRegistration.find({valueChainOperatorID: req.user.valueChainOperatorID}, function (err, productionRegistrations) {
            if (productionRegistrations) {
                res.status(process.env.SUCCESS).json(productionRegistrations)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Production/Harvest not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

};
