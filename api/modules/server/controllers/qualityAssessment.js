//const express = require('express');
//const router = express.Router();
const Lot = require('../../mongoose/orm/lot');
const QualityAssessment = require('../../mongoose/orm/qualityAssessment');
const ValueChainOperator = require('../../mongoose/orm/valueChainOperator');
const EventLog = require('../../mongoose/orm/eventLog');
const Event = require('../controllers/event');
module.exports = {

//Method to get all the entries from the table "QualityAssessment" from the database
    getAll: function (req, res) {
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (lot) {
                req.body['lotID'] = lot[0]._id;
                QualityAssessment.find(function (err, qualityassessment) {
                    if (qualityassessment && qualityassessment.length > 0) {
                        res.status(process.env.SUCCESS).json(qualityassessment)
                    } else {
                        res.status(process.env.NOT_FOUND).json({'message': 'No Quality Assement inserted yet.'})
                    }
                });
            }

        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to search an entrie on the table "QualityAssessment" by ID
    getById: function (req, res) {
        QualityAssessment.findById(req.params.id, function (err, qualityassessment) {
            if (qualityassessment) {
                res.status(process.env.SUCCESS).json(qualityassessment)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Quality Assements not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

    //Method to search an entrie on the table "QualityAssessment" by the value Chain Operator associated with the user
    getByValueChainOperator: function (req, res) {
        QualityAssessment.find({valueChainOperatorID: req.user.valueChainOperatorID}, function (err, qualityassessment) {
            if (qualityassessment && qualityassessment.length > 0) {
                res.status(process.env.SUCCESS).json(qualityassessment)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Quality Assessments inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to post a entrie on the table "QualityAssessment"
    create: function (req, res) {
        let qualityAssessment = {};
        let newLot = {}
        let eventLog = {};
        newLot['unit'] = req.body.unit;
        newLot['creationDate'] = req.body.lotCreationDate;
        newLot['expirationDate'] = req.body.expirationDate;
        newLot['valueChainOperatorID'] = req.user.valueChainOperatorID;

        qualityAssessment['longitude'] = req.body.longitude;
        qualityAssessment['latitude'] = req.body.latitude;
        qualityAssessment['description'] = req.body.description;
        qualityAssessment['qualityIndicator'] = req.body.qualityIndicator;
        qualityAssessment['temperature'] = req.body.temperature;
        qualityAssessment['weight'] = req.body.weight;
        qualityAssessment['userID'] = req.user._id;
        qualityAssessment['valueChainOperatorID'] = req.user.valueChainOperatorID;

        eventLog['userId'] = req.user._id;
        eventLog["eventName"] = "Quality Assessment";
        Lot.find({lotNumber: req.body.lotNumber}, async function (err, lot) {
            console.log(lot)
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            if (lot && lot.length > 0) {
                qualityAssessment['lotID'] = lot[0]._id;
                newLot['typeOfHarvestID'] = lot[0].typeOfHarvestId
                newLot['productID'] = lot[0].productID;
                newLot['weight'] = req.body.weight;
                qualityAssessment['weight'] = lot[0].weight;
                if (req.body.newLotNumber) {
                    await ValueChainOperator.findById(req.user.valueChainOperatorID, async function (err, valuechainoperator) {
                        if (err) return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                        console.log(valuechainoperator.abbreviation)
                        if (valuechainoperator) {
                            newLot['lotNumber'] = valuechainoperator.abbreviation + '-' + req.body.newLotNumber;
                            await Lot.create(newLot, function (err, lot) {
                                if (lot != null) {
                                    qualityAssessment['newLotID'] = lot._id;
                                    QualityAssessment.create(qualityAssessment, async function (err, qualityassessment) {
                                        if (err) return res.status(err.status || process.env.ERROR).json({
                                            'message': err || 'Some error occurred.'
                                        });
                                        await Event.updateEditableForAllEvents(qualityassessment.lotID, qualityassessment._id);
                                        await Event.updateEditableForAllEvents(qualityassessment.newLotID, qualityassessment._id);
                                        eventLog["eventId"] = qualityassessment._id;
                                        eventLog["message"] = "Event Qaulity Assessment save successfully"
                                        EventLog.create(eventLog, function (err, eventLog) {
                                            res.status(process.env.SUCCESS).json(qualityassessment);
                                        })
                                    });
                                }
                                eventLog["message"] = "Error when try create lot for the Quality Assessment event";
                                EventLog.create(eventLog, function (err, eventLog) {
                                })
                            });
                        }
                    });
                } else {
                    QualityAssessment.create(qualityAssessment, async function (err, qualityassessment) {
                        if (err) return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                        await Event.updateEditableForAllEvents(qualityassessment.lotID, qualityassessment._id);
                        eventLog["eventId"] = qualityassessment._id;
                        eventLog["message"] = "Event save successfully"
                        EventLog.create(eventLog, function (err, eventLog) {
                            res.status(process.env.SUCCESS).json(qualityassessment);
                        })
                    });
                }
            }

        });
    },

//Method to edit a entrie on the table "QualityAssessment" by ID
    update: function (req, res) {
        let qualityAssessment = {};
        let newLot = {}
        newLot['unit'] = req.body.unit;
        newLot['creationDate'] = req.body.lotCreationDate;
        newLot['expirationDate'] = req.body.expirationDate;
        newLot['valueChainOperatorID'] = req.user.valueChainOperatorID;

        qualityAssessment['longitude'] = req.body.longitude;
        qualityAssessment['latitude'] = req.body.latitude;
        qualityAssessment['geographicZone'] = req.body.geographicZone;
        qualityAssessment['description'] = req.body.description;
        qualityAssessment['qualityIndicator'] = req.body.qualityIndicator;
        qualityAssessment['temperature'] = req.body.temperature;
        qualityAssessment['weight'] = req.body.weight;
        qualityAssessment['userID'] = req.user._id;

        qualityAssessment['valueChainOperatorID'] = req.user.valueChainOperatorID;
        if (req.body.newLotID)
            Lot.findByIdAndUpdate(req.body.newLotID, newLot, function (err, lot) {
                console.log(err)
                if (lot != null) {
                }
            });
        QualityAssessment.findByIdAndUpdate(req.params.id, qualityAssessment, function (err, qualityassessment) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(qualityassessment);
        });
    },

//Method to delete a entrie on the table "QualityAssessment" by ID
    delete:
        function (req, res) {
            QualityAssessment.findByIdAndRemove(req.params.id, req.body, function (err, qualityassessment) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                if (qualityassessment.newLotID)
                    Lot.findByIdAndRemove(qualityassessment.newLotID, function (err, qualityassessment) {
                        res.status(process.env.SUCCESS).json(qualityassessment);
                    });
                else
                    res.status(process.env.SUCCESS).json(qualityassessment);
            });
        }

}
;
