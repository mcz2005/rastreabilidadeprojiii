//const express = require('express');
//const router = express.Router();
const Lot = require('../../mongoose/orm/lot');
const Sale = require('../../mongoose/orm/sale');
const EventLog = require('../../mongoose/orm/eventLog');
const LotController = require('../controllers/lot')
const Event = require('../controllers/event')
module.exports = {

//Method to get all the entries from the table "Sale" from the database
    getAll: function (req, res) {
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (lot && lot.length > 0) {
                req.body['lotID'] = lot[0]._id;
                Sale.find(function (err, sale) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    if (sale && sale.length > 0) {
                        res.status(process.env.SUCCESS).json(sale)
                    } else {
                        res.status(process.env.NOT_FOUND).json({'message': 'No Sells inserted yet.'})
                    }
                });
            }

        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },


//Method to search an entrie on the table "Sale" by ID
    getById: function (req, res) {
        Sale.findById(req.params.id, function (err, sale) {
            if (sale) {
                res.status(process.env.SUCCESS).json(sale)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Sale not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

    getByValueChainOperatorId: function (req, res) {
        Sale.find({valueChainOperatorID: req.user.valueChainOperatorID}, function (err, sale) {
            if (sale) {
                res.status(process.env.SUCCESS).json(sale)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Production/Harvest not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to post a entrie on the table "Sale"
    create: function (req, res) {
        req.body['userID'] = req.user._id;
        req.body['valueChainOperatorID'] = req.user.valueChainOperatorID;
        let eventLog = {};
        eventLog['userId'] = req.user._id;
        eventLog["eventName"] = "Sale";
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            if (lot && lot.length > 0 && LotController.isLotWeightCorrect(lot[0], req.body.weight)) {
                req.body['lotID'] = lot[0]._id;
                Sale.create(req.body, async function (err, sale) {
                    if (err) {
                        eventLog["message"] = "Error when try create Sale Event";
                        EventLog.create(eventLog, function (err, eventLog) {
                        })
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    await Event.updateEditableForAllEvents(sale.lotID, sale._id);
                    eventLog["eventId"] = sale._id;
                    eventLog["message"] = "Event Sale save successfully"
                    EventLog.create(eventLog, function (err, eventLog) {
                        res.status(process.env.SUCCESS).json(sale);
                    })
                });
            }
        });
    },

//Method to edit a entrie on the table "Sale" by ID
    update: function (req, res) {
        Sale.findByIdAndUpdate(req.params.id, req.body, function (err, sale) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(sale);
        });
    },

//Method to delete a entrie on the table "Sale" by ID
    delete: function (req, res) {
        Sale.findByIdAndRemove(req.params.id, req.body, function (err, sale) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(sale);
        });
    }

};
