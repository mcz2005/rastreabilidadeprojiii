//const express = require('express');
//const router = express.Router();

const Storing = require('../../mongoose/orm/storing');
const Lot = require('../../mongoose/orm/lot');
const EventLog = require('../../mongoose/orm/eventLog');
const Event = require('../controllers/event')
const LotController = require('./lot')
module.exports = {

//Method to get all the entries from the table "Storing" from the database
    getAll: function (req, res) {
        Storing.find(function (err, storing) {
            if (storing && storing.length > 0) {
                res.status(process.env.SUCCESS).json(storing)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Storing inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to search an entrie on the table "Storing" by ID
    getById: function (req, res) {
        Storing.findById(req.params.id, function (err, storing) {
            if (storing) {
                res.status(process.env.SUCCESS).json(storing)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Storing not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

    //Method to search an entrie on the table "Storgin" by the value Chain Operator associated with the user
    getByValueChainOperator: function (req, res) {
        Storing.find({valueChainOperatorID: req.user.valueChainOperatorID}, function (err, storing) {
            if (storing && storing.length > 0) {
                res.status(process.env.SUCCESS).json(storing)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Storing inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },


//Method to post a entrie on the table "Storing"
    create: function (req, res) {
        req.body['userID'] = req.user._id;
        req.body['valueChainOperatorID'] = req.user.valueChainOperatorID;
        let eventLog = {};
        eventLog['userId'] = req.user._id;
        eventLog["eventName"] = "Storing";
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (err) {
                return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
            }
            if (lot && lot.length > 0 && LotController.isLotWeightCorrect(lot[0], req.body.weight)) {
                req.body['lotID'] = lot[0]._id;
                Storing.create(req.body, async function (err, storing) {
                    if (err) {
                        eventLog["message"] = "Error when try create Storing Event";
                        EventLog.create(eventLog, function (err, eventLog) {
                        })
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    await Event.updateEditableForAllEvents(storing.lotID, storing._id);
                    eventLog["eventId"] = storing._id;
                    eventLog["message"] = "Event Storing save successfully"
                    EventLog.create(eventLog, function (err, eventLog) {
                        res.status(process.env.SUCCESS).json(storing);
                    })
                });
            }
        });
    },

//Method to edit a entrie on the table "Storing" by ID
    update: function (req, res) {
        Storing.findByIdAndUpdate(req.params.id, req.body, function (err, storing) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(storing);
        });
    },

//Method to delete a entrie on the table "Storing" by ID
    delete: function (req, res) {
        Storing.findByIdAndRemove(req.params.id, req.body, function (err, storing) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(storing);
        });
    }


};
