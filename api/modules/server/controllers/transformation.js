//const express = require('express');
//const router = express.Router();

const Transformation = require('../../mongoose/orm/transformation');
const Lot = require('../../mongoose/orm/lot');
const Event = require('../controllers/event');
const EventLog = require('../../mongoose/orm/eventLog');
const ValueChainOperator = require('../../mongoose/orm/valueChainOperator')
module.exports = {

//Method to get all the entries from the table "Transformation" from the database
    getAll: function (req, res) {
        Transformation.find(function (err, transformation) {
            if (transformation && transformation.length > 0) {
                res.status(process.env.SUCCESS).json(transformation)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Transformations inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to search an entrie on the table "Transformation" by ID
    getById: function (req, res) {
        Transformation.findById(req.params.id, function (err, transformation) {
            if (transformation) {
                res.status(process.env.SUCCESS).json(transformation)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Transformation not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },
    //Method to search an entrie on the table "Storgin" by the value Chain Operator associated with the user
    getByValueChainOperator: function (req, res) {
        Transformation.find({valueChainOperatorID: req.user.valueChainOperatorID}, function (err, transformation) {
            if (transformation && transformation.length > 0) {
                res.status(process.env.SUCCESS).json(transformation)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Storing inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to post a entrie on the table "Transformation"
    create: function (req, res) {
        let transformation = {};
        let lotToCreate = {};
        let eventLog = {};
        eventLog['userId'] = req.user._id;
        eventLog["eventName"] = "Transformation";
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (lot && lot.length > 0) {
                lotToCreate['productID'] = lot[0].productID;
                lotToCreate['typeOfHarvestID'] = lot[0].typeOfHarvestID;
                lotToCreate['unit'] = req.body.unit;
                lotToCreate['weight'] = req.body.weight;
                lotToCreate['creationDate'] = req.body.lotCreationDate;
                lotToCreate['expirationDate'] = req.body.expirationDate;
                lotToCreate['valueChainOperatorID'] = req.user.valueChainOperatorID

                transformation['longitude'] = req.body.longitude;
                transformation['lotID'] = lot[0]._id;
                transformation['latitude'] = req.body.latitude;
                transformation['description'] = req.body.description;
                transformation['weight'] = req.body.weight;
                transformation['userID'] = req.user._id;
                transformation['valueChainOperatorID'] = req.user.valueChainOperatorID;
                ValueChainOperator.findById(req.user.valueChainOperatorID, function (err, valuechainoperator) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    if (valuechainoperator) {
                        lotToCreate['lotNumber'] = valuechainoperator.abbreviation + '-' + req.body.newLotNumber;
                        Lot.create(lotToCreate, function (err, lot) {
                            if (err) {
                                eventLog["message"] = "Error when try create new Lot for the Transformation Event ";
                                EventLog.create(eventLog, function (err, eventLog) {
                                })
                                return res.status(err.status || process.env.ERROR).json({
                                    'message': err || 'Some error occurred.'
                                });
                            } else {
                                transformation['newLotID'] = lot._id;
                                Transformation.create(transformation, async function (err, transformation) {
                                    if (err) {
                                        eventLog["message"] = "Error when try create Transformation Event";
                                        EventLog.create(eventLog, function (err, eventLog) {
                                        })
                                        return res.status(err.status || process.env.ERROR).json({
                                            'message': err || 'Some error occurred.'
                                        });
                                    }
                                    await Event.updateEditableForAllEvents(transformation.lotID, transformation._id);
                                    await Event.updateEditableForAllEvents(transformation.newLotID, transformation._id);
                                    eventLog["eventId"] = transformation._id;
                                    eventLog["message"] = "Event Transformation save successfully"
                                    EventLog.create(eventLog, function (err, eventLog) {
                                        res.status(process.env.SUCCESS).json(transformation);
                                    })
                                });
                            }
                        });
                    }
                });
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });

    },

//Method to edit a entrie on the table "Transformation" by ID
    update: function (req, res) {
        let transformation = {};
        let lotToCreate = {};
        lotToCreate['unit'] = req.body.unit;
        lotToCreate['weight'] = req.body.weight;
        lotToCreate['creationDate'] = req.body.lotCreationDate;
        lotToCreate['expirationDate'] = req.body.expirationDate;

        transformation['longitude'] = req.body.longitude;
        transformation['latitude'] = req.body.latitude;
        transformation['description'] = req.body.description;
        transformation['weight'] = req.body.weight;
        Lot.findByIdAndUpdate(req.body.newLotID, lotToCreate, function (err, lot) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            else {
                Transformation.findByIdAndUpdate(req.body._id, transformation, async function (err, transformation) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    await Event.updateEditableForAllEvents(transformation.lotID, transformation._id);
                    res.status(process.env.SUCCESS).json(transformation);
                });
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to delete a entrie on the table "Transformation" by ID
    delete:
        function (req, res) {
            Transformation.findByIdAndRemove(req.params.id, req.body, function (err, transformation) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                Lot.findByIdAndRemove(transformation.newLotID, function (err, lot) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    res.status(process.env.SUCCESS).json(transformation);
                });
            });
        }


}
;
