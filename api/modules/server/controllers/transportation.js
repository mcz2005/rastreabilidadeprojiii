//const express = require('express');
//const router = express.Router();
const Lot = require('../../mongoose/orm/lot');
const Transportation = require('../../mongoose/orm/transportation');
const Event = require('../controllers/event');
const EventLog = require('../../mongoose/orm/eventLog');
const LotController = require('./lot')
module.exports = {

//Method to get all the entries from the table "Transportation" from the database
    getAll: function (req, res) {
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (lot) {
                req.body['lotID'] = lot[0]._id;
                Transportation.find(function (err, transportation) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                    if (transportation && transportation.length > 0) {
                        res.status(process.env.SUCCESS).json(transportation)
                    } else {
                        res.status(process.env.NOT_FOUND).json({'message': 'No Transportation inserted yet.'})
                    }
                });
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to search an entrie on the table "Transportation" by ID
    getById: function (req, res) {
        Transportation.findById(req.params.id, function (err, transportation) {
            if (transportation) {
                res.status(process.env.SUCCESS).json(transportation)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Transportation not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to post a entrie on the table "Transportation"
    create: function (req, res) {
        req.body['userID'] = req.user._id;
        req.body['valueChainOperatorID'] = req.user.valueChainOperatorID;
        let eventLog = {};
        eventLog['userId'] = req.user._id;
        eventLog["eventName"] = "Transportation";
        Lot.find({lotNumber: req.body.lotNumber}, function (err, lot) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            if (lot && lot.length > 0 && LotController.isLotWeightCorrect(lot[0], req.body.weight)) {
                req.body['lotID'] = lot[0]._id;
                Transportation.create(req.body, async function (err, transportation) {
                    if (err) {
                        eventLog["message"] = "Error creating Transportation Event";
                        EventLog.create(eventLog, function (err, eventLog) {
                        })
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    await Event.updateEditableForAllEvents(transportation.lotID, transportation._id);
                    eventLog["eventId"] = transportation._id;
                    eventLog["message"] = "Transformation Event saved successfully"
                    EventLog.create(eventLog, function (err, eventLog) {
                        res.json(transportation);
                    })
                });
            }
        })
    },

//Method to edit a entrie on the table "Transportation" by ID
    update: function (req, res) {
        Transportation.findByIdAndUpdate(req.params.id, req.body, function (err, transportation) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(transportation);
        });
    },

//Method to delete a entrie on the table "Transportation" by ID
    delete: function (req, res) {
        Transportation.findByIdAndRemove(req.params.id, req.body, function (err, transportation) {
            if (err) return res.status(err.status || process.env.ERROR).json({
                'message': err || 'Some error occurred.'
            });
            res.status(process.env.SUCCESS).json(transportation);
        });
    },

    //Method to search an entrie on the table "Transportation" by ID
    getByValueChainOperatorId: function (req, res) {
        Transportation.find({valueChainOperatorID: req.user.valueChainOperatorID}, function (err, transportation) {
                if (transportation) {
                    res.status(process.env.SUCCESS).json(transportation)
                } else {
                    res.status(process.env.NOT_FOUND).json({'message': 'Transportation not found.'})
                }
            }
        ).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    }
};
