//const express = require('express');
//const router = express.Router();
const TypeOfHarvest = require('../../mongoose/orm/typeOfHarvest');

module.exports = {

//Method to get all the entries from the table "TypeOfHarvest" from the database
    getAll: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {

            TypeOfHarvest.find(function (err, typeOfHarvest) {
                if (typeOfHarvest && typeOfHarvest.length > 0) {
                    res.status(process.env.SUCCESS).json(typeOfHarvest)
                }
                else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Types of Harvest inserted yet.'})
                }
            }).catch(err => {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                })
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to get all the available entries from the table "ProductType" from the database
    getAvailable: function (req, res) {
        TypeOfHarvest.find({available: true}, function (err, typeOfHarvest) {
            if (typeOfHarvest && typeOfHarvest.length > 0) {
                res.status(process.env.SUCCESS).json(typeOfHarvest);
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Availabe Product Types inserted'});
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred'
            });
        })
    },

    //Method to search an entrie on the table "TypeOfHarvest" by ID
    getById: function (req, res) {
        TypeOfHarvest.findById(req.params.typeOfHarvestId, function (err, typeOfHarvest) {
            if (typeOfHarvest) {
                res.status(process.env.SUCCESS).json(typeOfHarvest)
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'Types of Harvest not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    }
    ,


    //Method to post a entrie on the table "TypeOfHarvest"
    create: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            TypeOfHarvest.create(req.body, function (err, typeOfHarvest) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(typeOfHarvest);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    }
    ,

    //Method to edit a entrie on the table "TypeOfHarvest" by ID
    update: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            TypeOfHarvest.findByIdAndUpdate(req.params.id, req.body, function (err, typeOfHarvest) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(typeOfHarvest);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    }
    ,

    //Method to delete a entrie on the table "TypeOfHarvest" by ID
    updateAvailability: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            TypeOfHarvest.findById(req.params.id, function (err, typeOfHarvest) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                typeOfHarvest.available = !typeOfHarvest.available
                typeOfHarvest.save(function (err) {
                    if (err) {
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    res.status(process.env.SUCCESS).json(typeOfHarvest);
                });
            });
        } else {
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
        }



    }
};