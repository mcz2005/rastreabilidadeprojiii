//const express = require('express');
//const router = express.Router();
const TypeOfValueChainOperator = require('../../mongoose/orm/typeOfValueChainOperator');

module.exports = {

//Method to get all the entries from the table "TypeOfValueChainOperator" from the database
    getAll: function (req, res) {
        console.log(req.headers)
        console.log(res.headers)
        if (req.user.role.toLowerCase() === 'admin') {
            TypeOfValueChainOperator.find(function (err, typeOfValueChainOperator) {
                    if (typeOfValueChainOperator && typeOfValueChainOperator.length > 0) {
                        res.status(process.env.SUCCESS).json(typeOfValueChainOperator)
                    } else {
                        res.status(process.env.NOT_FOUND).json({'message': 'No Types of Value Chain Operator inserted yet.'})
                    }
            }).catch(err => {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                })
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to get all the available entries from the table "TypeOfValueChainOperator" from the database
    getAvailable: function (req, res) {
        TypeOfValueChainOperator.find({available: true}, function (err, typeOfValueChainOperator) {
                if (typeOfValueChainOperator && typeOfValueChainOperator.length > 0) {
                    res.status(process.env.SUCCESS).json(typeOfValueChainOperator);
                } else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Availabe Value Chain Operators Types inserted'});
                }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

    //Method to search an entrie on the table "TypeOfValueChainOperator" by ID
    getById: function (req, res) {
        TypeOfValueChainOperator.findById(req.params.id, function (err, typeOfValueChainOperator) {
            if (typeOfValueChainOperator) {
                res.status(process.env.SUCCESS).json(typeOfValueChainOperator)
            } else {
                res.status(process.env.NOT_FOUND).json({'message': 'Types of Value Chain Operator not found.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    }
    ,


    //Method to post a entrie on the table "TypeOfValueChainOperator"
    create: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            TypeOfValueChainOperator.create(req.body, function (err, typeOfValueChainOperator) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(typeOfValueChainOperator);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    }
    ,

    //Method to edit a entrie on the table "TypeOfValueChainOperator" by ID
    update: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            TypeOfValueChainOperator.findByIdAndUpdate(req.params.id, req.body, function (err, typeOfValueChainOperator) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(typeOfValueChainOperator);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    }
    ,

    //Method to delete a entrie on the table "TypeOfValueChainOperator" by ID
    updateAvailability: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            TypeOfValueChainOperator.findById(req.params.id, function (err, typeOfValueChainOperator) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                typeOfValueChainOperator.available = !typeOfValueChainOperator.available
                typeOfValueChainOperator.save(function (err) {
                    if (err) {
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    res.status(process.env.SUCCESS).json(typeOfValueChainOperator);
                });
            });
        } else {
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
        }
    }
};
