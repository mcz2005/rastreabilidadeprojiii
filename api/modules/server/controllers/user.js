//const express = require('express');
//const router = express.Router();
const User = require('../../mongoose/orm/user');
const ValueChainOperator = require('../../mongoose/orm/valueChainOperator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

//Method to get all the entries from the table "User" from the database
    getAll: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            User.find(function (err, user) {
                if (user && user.length > 0) {

                    res.status(process.env.SUCCESS).json(user)
                } else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Users inserted yet.'})
                }
            }).catch(err => {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                })
            });
        } else
            return res.status(process.env.UNAUTHORIZED).json('Unauthorized');
    },

    getAvailable: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            User.find({available: true}, function (err, user) {
                if (user && user.length > 0) {
                    res.status(process.env.SUCCESS).json(user);
                } else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Available Products inserted'})
                }
            }).catch(function (err) {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                });
            });
        } else
            return res.status(process.env.UNAUTHORIZED).json('Unauthorized');
    },

    getRolesFromAllUsers: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            User.find({}, function (err, users) {

                if (users && users.length > 0) {
                    users = users.map(user => {
                        return {
                            role: user.role,
                            available: user.available
                        }
                    })
                    res.status(process.env.SUCCESS).json(users);
                } else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Available Users inserted'})
                }
            }).catch(function (err) {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                });
            });
        } else
            return res.status(process.env.UNAUTHORIZED).json('Unauthorized');
    },

//Method to search an entrie on the table "User" by ID
    getById: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            User.findById(req.params.id, function (err, user) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                if (user) {
                    res.status(process.env.SUCCESS).json(user)
                } else {
                    res.status(process.env.NOT_FOUD).json({'message': 'User not found.'});
                }
            })
        } else
            return res.status(process.env.UNAUTHORIZED).json('Unauthorized');
    },

//Method to edit a entrie on the table "User" by ID
    update: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin' || req.user.role.toLowerCase() === 'worker admin') {
            User.findByIdAndUpdate(req.params.id, req.body, function (err, user) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                res.status(process.env.SUCCESS).json(user);
            });
        } else
            return res.status(process.env.UNAUTHORIZED).json('Unauthorized');
    },

//Method to delete a entrie on the table "User" by ID
    delete: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin' || req.user.role.toLowerCase() === 'worker admin') {
            User.findById(req.params.id, function (err, user) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                user.available = !user.available;
                user.save(function (err) {
                    if (err) {
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    res.status(process.env.SUCCESS).json(user);
                });
            });
        } else {
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
        }
    },

//Method to create a new user (verify if username exists, and if it doesn't exists, an new hash is
//generated to store the password and if it results, the new user is created with an encrypted password
    register: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin' || req.user.role.toLowerCase() == 'worker admin') {
            User.findOne({username: req.body.username}, function (err, user) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                });

                if (user) return res.status(process.env.UNAUTHORIZED).json({
                    'message': 'Username Already exists'
                });

                bcrypt.hash(req.body.password, bcrypt.genSaltSync(10), function (err, hash) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err.message || 'Some error occurred.'
                    });

                    req.body.password = hash;
                    User.create(req.body, function (err, user) {
                        if (err) return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                        res.status(process.env.SUCCESS).json(user);
                    });
                });

            });
        } else
            return res.status(process.env.UNAUTHORIZED).json('Unauthorized');
    },

//Método de logIn utilizado para verificar se o username existe na base de dados. Se existir
//virifica-se se a password iserida encriptada é igual a encriptação na base de dados
//Se a password existir, gera-se um token com o id do user logado e username, defenindo-se a chave de
//encriptação e o respetivo tempo de expiração
    login: function (req, res) {
        return User.findOne({username: req.body.username}, function (err, user) {
                if (err) return res.status(err.status || process.env.UNAUTHORIZED).json({
                    'message': err.message || 'Username and password dont match'
                });
                if (user) {
                    if (user.available === false) {
                        return res.status(process.env.UNAUTHORIZED).json({
                            'message': 'Your user are unavailable please contact the system admin '
                        });
                    }
                    if (user.role.toLowerCase() !== 'admin') {
                         ValueChainOperator.findById(user.valueChainOperatorID, function (err, valueChainOperator) {
                            if (valueChainOperator.available === false) {
                                return res.status(process.env.UNAUTHORIZED).json({
                                    'message': 'Your Value Chain are unavailable please contact the system admin '
                                });
                            }
                        }).catch(err => {
                             res.status(err.status || process.env.ERROR).json({
                                 'message': err.message || 'Some error occurred.'
                             })
                         });
                    }
                    bcrypt.compare(req.body.password, user.password)
                        .then(result => {
                            if (result) {
                                let token = jwt.sign(
                                    {
                                        id: user._id,
                                        username: user.username
                                    },
                                    process.env.JWT_SECRET,
                                    {expiresIn: '10m'});
                                res.setHeader('Authorization', token);
                                return res.status(process.env.SUCCESS).json({
                                    'token': token,
                                    'user': user
                                })
                            }

                            return res.status(process.env.UNAUTHORIZED).json({
                                'message': 'Username and password don\'t match'
                            });
                        })
                }
                if (!user) {
                    return res.status(process.env.UNAUTHORIZED).json({
                        'message': 'Username and password don\'t match'
                    });
                }
            }
        );
    },


    getByValueChainOperatorId: function (req, res) {
        if (req.user.role.toLowerCase() === 'worker admin') {
            User.find({valueChainOperatorID: req.user.valueChainOperatorID, available: true}, function (err, user) {
                if (user && user.length > 0) {
                    res.status(process.env.SUCCESS).json(user);
                } else {
                    res.status(process.env.NOT_FOUND).json({'message': 'No Available Products inserted'})
                }
            }).catch(function (err) {
                res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                });
            });
        } else
            return res.status(process.env.UNAUTHORIZED).json('Unauthorized');
    },
};
