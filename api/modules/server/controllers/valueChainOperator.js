//const express = require('express');
//const router = express.Router();
const ValueChainOperator = require('../../mongoose/orm/valueChainOperator');
const OperatorType = require('../../mongoose/orm/operatorType');

module.exports = {

//Method to get all the entries from the table "ValueChainOperator" from the database
    getAll: function (req, res) {
        ValueChainOperator.find(function (err, valueChainOperator) {
            if (valueChainOperator && valueChainOperator.length > 0) {
                res.status(process.env.SUCCESS).json(valueChainOperator)
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Value Chain Operator inserted yet.'})
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred.'
            })
        });
    },

//Method to get all the available entries from the table "ValueChainOperator" from the database
    getAvailable: function (req, res) {
        ValueChainOperator.find({available: true}, function (err, valueChainOperator) {
            if (valueChainOperator && valueChainOperator.length > 0) {
                res.status(process.env.SUCCESS).json(valueChainOperator);
            }
            else {
                res.status(process.env.NOT_FOUND).json({'message': 'No Availabe Chain Operators inserted'});
            }
        }).catch(err => {
            res.status(err.status || process.env.ERROR).json({
                'message': err.message || 'Some error occurred'
            });
        })
    },

//Method to search an entrie on the table "ValueChainOperator" by ID
    getById: function (req, res) {
            ValueChainOperator.findById(req.params.id, function (err, valueChainOperator) {
                if (valueChainOperator) {
                    return res.status(process.env.SUCCESS).json(valueChainOperator)
                }
                else {
                    return res.status(process.env.NOT_FOUND).json({'message': 'Value Chain Operator not found.'})
                }
            }).catch(err => {
                return res.status(err.status || process.env.ERROR).json({
                    'message': err.message || 'Some error occurred.'
                })
            });
    },

//Method to post a entrie on the table "ValueChainOperator"
    create: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            let opertatorTypes = req.body.types;
            delete req.body.type;
            ValueChainOperator.create(req.body, function (err, valueChainOperator) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                opertatorTypes.forEach(type => {
                    OperatorType.create(
                        {
                            typeOfValueChainOperatorID: type._id,
                            valueChainOperatorID: valueChainOperator._id
                        }, operatorType => {
                        })
                });
                res.json(valueChainOperator);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to edit a entrie on the table "ValueChainOperator" by ID
    update: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            let opertatorTypesEdited = req.body.types;
            delete req.body.type;
            ValueChainOperator.findByIdAndUpdate(req.params.id, req.body, function (err, valueChainOperator) {
                if (err) return res.status(err.status || process.env.ERROR).json({
                    'message': err || 'Some error occurred.'
                });
                OperatorType.find({valueChainOperatorID: req.params.id}, function (err, operatorTypes) {
                    if (err) return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });

                    opertatorTypesEdited.forEach(typeEdited => {
                        let exists = false;
                        operatorTypes.forEach(oldType => {
                            if (typeEdited._id == oldType.typeOfValueChainOperatorID) {
                                if (!oldType.available) {
                                    oldType.available = true;
                                    OperatorType.findOneAndUpdate({_id: oldType._id},oldType, function (err, valueChainOperator) {
                                        if (err) return res.status(err.status || process.env.ERROR).json({
                                            'message': err || 'Some error occurred.'
                                        });
                                    });
                                }
                                exists = true;
                            }
                        });

                        if(!exists) {
                            OperatorType.create({
                                typeOfValueChainOperatorID: typeEdited._id,
                                valueChainOperatorID: valueChainOperator._id
                            }, operatorType => {

                            })
                        }
                    });
                    operatorTypes.forEach(oldType => {
                        let exists = false;
                        opertatorTypesEdited.forEach(typeEdited => {
                            if(typeEdited._id == oldType.typeOfValueChainOperatorID) {
                                exists = true;
                            }
                        });
                        if(!exists) {
                            if(oldType.available) {
                                oldType.available = false;
                                OperatorType.findOneAndUpdate({_id: oldType._id}, oldType, function (err, valueChainOperator) {
                                    if (err) return res.status(err.status || process.env.ERROR).json({
                                        'message': err || 'Some error occurred.'
                                    });
                                });
                            }
                        }
                    });

                })

                res.status(process.env.SUCCESS).json(valueChainOperator);
            });
        } else
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
    },

//Method to delete a entrie on the table "ValueChainOperator" by ID
    updateAvailability: function (req, res) {
        if (req.user.role.toLowerCase() === 'admin') {
            ValueChainOperator.findById(req.params.id, function (err, valueChainOperator) {
                if (err) {
                    return res.status(err.status || process.env.ERROR).json({
                        'message': err || 'Some error occurred.'
                    });
                }
                valueChainOperator.available = !valueChainOperator.available
                valueChainOperator.save(function (err) {
                    if (err) {
                        return res.status(err.status || process.env.ERROR).json({
                            'message': err || 'Some error occurred.'
                        });
                    }
                    res.status(process.env.SUCCESS).json(valueChainOperator);
                });
            });
        } else {
            return res.status(process.env.FORBIDDEN).json('Unauthorized');
        }
    }

}


