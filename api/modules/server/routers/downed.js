const express = require('express');
const router = express.Router();
const Downed = require('../controllers/downed');
const passport = require('passport');

require('../config/passport')(passport);

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Downed.getAll(req, res)
});
router.get('/getbyvaluechainoperatorid', passport.authenticate('jwt', {session: false}), function (req, res) {
    Downed.getByValueChainOperator(req, res)
});
router.get('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Downed.getById(req, res)
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Downed.create(req, res)
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Downed.update(req, res)
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Downed.delete(req, res)
});

module.exports = router;
