const express = require('express');
const router = express.Router();
const Event = require('../controllers/event');

/**
 * @swagger
 * /users:
 *    get:
 *      description: This should return all users
 */
router.get('/lot/:lotNumber', function (req, res) {
    Event.getTraceability(req, res)
});

module.exports = router;
