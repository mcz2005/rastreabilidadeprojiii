const express = require('express');
const router = express.Router();
const EventLog = require('../controllers/eventLog');
const passport = require('passport');

require('../config/passport')(passport);

router.get('/byUser', passport.authenticate('jwt', {session: false}), function (req, res) {
    EventLog.getbyUser(req, res)
});
module.exports = router;
