const lot = require('./lot');
const product = require('./product');
const valuechainoperator = require('./valueChainOperator');
const typeofharvest = require('./typeOfHarvest');
const producttype = require('./productType');
const user = require('./user');
const producttypeofharvest = require('./productTypeOfHarvest');
const operatortype = require('./operatorType');
const typeofvaluechainoperator = require('./typeOfValueChainOperator')
const eventLog = require('./eventLog')

//Events routers

const downed = require('./downed');
const event = require('./event');
const productionregistration = require('./productionRegistration');
const qualityassessment = require('./qualityAssessment');
const sale = require('./sale');
const storing = require('./storing');
const transformation = require('./transformation');
const transportation = require('./transportation');

const jwt = require('jsonwebtoken');

module.exports = (router, app) => {

    app.get('/*', (req, res, next) => {
        let token = req.headers.authorization ? req.headers.authorization.split('Bearer ')[1] : null;
        if (token && token != 'null') {
            let decode = jwt.decode(token);
            //Tempo restante do token
            let tokenTime = new Date(((new Date(decode.exp * 1000)).getTime() - Date.now())).getMinutes();
            if (tokenTime < 10) {
                let time = tokenTime + 10;
                let newToken = jwt.sign(
                    {
                        id: decode.id,
                        username: decode.username
                    },
                    process.env.JWT_SECRET,
                    {expiresIn: time + 'm'});
                res.setHeader('Authorization', newToken);
            } else {
                res.setHeader('Authorization', token);
            }
            next();
        } else {
            next();
        }
    });

    app.post('/*', (req, res, next) => {
        let token = req.headers.authorization ? req.headers.authorization.split('Bearer ')[1] : null;
        if (token && token != 'null') {
            let decode = jwt.decode(token);
            let tokenTime = new Date(((new Date(decode.exp * 1000)).getTime() - Date.now())).getMinutes();
            if (tokenTime < 10) {
                let time = tokenTime + 10;
                let newToken = jwt.sign(
                    {
                        id: decode.id,
                        username: decode.username
                    },
                    process.env.JWT_SECRET,
                    {expiresIn: time + 'm'});

                res.setHeader('Authorization', newToken);
            } else {
                res.setHeader('Authorization', token);
            }
        }

        next();
    });

    app.put('/*', (req, res, next) => {
        let token = req.headers.authorization ? req.headers.authorization.split('Bearer ')[1] : null;
        if (token) {
            let decode = jwt.decode(token);
            let tokenTime = new Date(((new Date(decode.exp * 1000)).getTime() - Date.now())).getMinutes();
            if (tokenTime < 10) {
                let time = tokenTime + 10;
                let newToken = jwt.sign(
                    {
                        id: decode.id,
                        username: decode.username
                    },
                    process.env.JWT_SECRET,
                    {expiresIn: time + 'm'});

                res.setHeader('Authorization', newToken);
            } else {
                res.setHeader('Authorization', token);
            }
        }

        next();
    });

    app.delete('/*', (req, res, next) => {
        let token = req.headers.authorization ? req.headers.authorization.split('Bearer ')[1] : null;
        if (token) {
            let decode = jwt.decode(token);
            let tokenTime = new Date(((new Date(decode.exp * 1000)).getTime() - Date.now())).getMinutes();
            if (tokenTime < 10) {
                let time = tokenTime + 10;
                let newToken = jwt.sign(
                    {
                        id: decode.id,
                        username: decode.username
                    },
                    process.env.JWT_SECRET,
                    {expiresIn: time + 'm'});

                res.setHeader('Authorization', newToken);
            } else {
                res.setHeader('Authorization', token);
            }
        }

        next();
    });

    app.use('/api/product', product);
    app.use('/api/lot', lot);
    app.use('/api/typeofharvest', typeofharvest);
    app.use('/api/producttype', producttype);
    app.use('/api/producttypeofharvest', producttypeofharvest);
    app.use('/api/user', user);
    app.use('/api/operatortype', operatortype);
    app.use('/api/typeofvaluechainoperator', typeofvaluechainoperator);
    app.use('/api/valuechainoperator', valuechainoperator);

    //Events routers
    app.use('/api/event', event);
    app.use('/api/qualityassessment', qualityassessment);
    app.use('/api/sale', sale);
    app.use('/api/storing', storing);
    app.use('/api/productionregistration', productionregistration);
    app.use('/api/downed', downed);
    app.use('/api/transformation', transformation);
    app.use('/api/transportation', transportation);
    app.use('/api/eventLog', eventLog)


    return router;

};
