const express = require('express');
const router = express.Router();
const Lot = require('../controllers/lot');
const passport = require('passport');

require('../config/passport')(passport);

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {Lot.getAll(req, res)});
router.get('/:id', function (req, res) {Lot.getById(req, res)});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {Lot.create(req, res)});
router.post('/getByLotNumber', passport.authenticate('jwt', {session: false}), function (req, res) {Lot.getByLotNumber(req, res)});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {Lot.update(req, res)});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {Lot.delete(req, res)});

module.exports = router;
