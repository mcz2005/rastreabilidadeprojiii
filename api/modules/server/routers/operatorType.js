const express = require('express');
const router = express.Router();
const OperatorType = require('../controllers/operatorType');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    OperatorType.getAll(req, res);
});
router.get('/available', function (req, res) {
    OperatorType.getAvailable(req, res);
});
router.get('/getbyvaluechainoperatorid/:valuechainOperatorid', function (req, res) {
    OperatorType.getByValueChainOperatorID(req, res);
});
router.get('/getById/:id', function (req, res) {
    OperatorType.getById(req, res);
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    OperatorType.create(req, res);
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    OperatorType.update(req, res);
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    OperatorType.delete(req, res);
});
module.exports = router;
