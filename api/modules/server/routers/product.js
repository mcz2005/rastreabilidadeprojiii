const express = require('express');
const router = express.Router();
const Product = require('../controllers/product');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Product.getAll(req, res);
});
router.get('/available', passport.authenticate('jwt', {session: false}), function (req, res) {
    Product.getAvailable(req, res);
});
router.get('/getbyid/:id', function (req, res) {
    Product.getById(req, res);
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Product.create(req, res);
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Product.update(req, res);
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Product.updateAvailability(req, res);
});


module.exports = router;