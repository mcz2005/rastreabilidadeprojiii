const express = require('express');
const router = express.Router();
const ProductType = require('../controllers/productType');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductType.getAll(req, res);
});
router.get('/getById/:productTypeID', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductType.getById(req, res);
});
router.get('/available', function (req, res) {
    ProductType.getAvailable(req, res);
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductType.create(req, res);
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductType.update(req, res);
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductType.updateAvailability(req, res);
});

module.exports = router;