const express = require('express');
const router = express.Router();
const ProductTypeOfHarvest = require('../controllers/productTypeOfHarvest');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductTypeOfHarvest.getAll(req, res);
});
router.get('/available', function (req, res) {
    ProductTypeOfHarvest.getAvailable(req, res);
});

router.get('/getbyproducttypeid/:producttypeid', function (req, res) {
    ProductTypeOfHarvest.getByProductTypeID(req, res);
});

router.get('/getById/:id', function (req, res) {
    ProductTypeOfHarvest.getById(req, res);
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductTypeOfHarvest.create(req, res);
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductTypeOfHarvest.update(req, res);
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductTypeOfHarvest.updateAvailability(req, res);
});
module.exports = router;
