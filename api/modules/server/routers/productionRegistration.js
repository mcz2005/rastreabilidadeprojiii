const express = require('express');
const router = express.Router();
const ProductionRegistration = require('../controllers/productionRegistration');
const passport = require('passport');

require('../config/passport')(passport);

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductionRegistration.getAll(req, res)
});
router.get('/getbyvaluechainoperatorid/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductionRegistration.getByValueChainOperatorId(req, res)
});
router.get('/getbyid/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductionRegistration.getById(req, res)
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductionRegistration.create(req, res)
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductionRegistration.update(req, res)
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ProductionRegistration.delete(req, res)
});

module.exports = router;
