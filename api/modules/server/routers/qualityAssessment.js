const express = require('express');
const router = express.Router();
const QualityAssessment = require('../controllers/qualityAssessment');
const passport = require('passport');

require('../config/passport')(passport);

router.get('/',  passport.authenticate('jwt', {session: false}), function (req, res) {
    QualityAssessment.getAll(req, res)
});
router.get('/getbyvaluechainoperatorid', passport.authenticate('jwt', {session: false}), function (req, res) {
    QualityAssessment.getByValueChainOperator(req, res)
});
router.get('/getbyid/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    QualityAssessment.getById(req, res)
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    QualityAssessment.create(req, res)
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    QualityAssessment.update(req, res)
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    QualityAssessment.delete(req, res)
});

module.exports = router;