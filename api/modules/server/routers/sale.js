const express = require('express');
const router = express.Router();
const Sale = require('../controllers/sale');
const passport = require('passport');
require('../config/passport')(passport);


router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Sale.getAll(req, res)
});
router.get('/getbyid/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Sale.getById(req, res)
});
router.get('/getbyvaluechainoperatorid/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Sale.getByValueChainOperatorId(req, res)
});
router.post('/',passport.authenticate('jwt', {session: false}), function (req, res) {
    Sale.create(req, res)
});
router.put('/:id',passport.authenticate('jwt', {session: false}), function (req, res) {
    Sale.update(req, res)
});
router.delete('/:id',passport.authenticate('jwt', {session: false}), function (req, res) {
    Sale.delete(req, res)
});

module.exports = router;