const express = require('express');
const router = express.Router();
const Storing = require('../controllers/storing');
const passport = require('passport');
require('../config/passport')(passport);


router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Storing.getAll(req, res)
});
router.get('/getbyid/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Storing.getById(req, res)
});
router.get('/getbyvaluechainoperatorid/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Storing.getByValueChainOperator(req, res)
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Storing.create(req, res)
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Storing.update(req, res)
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Storing.delete(req, res)
});

module.exports = router;
