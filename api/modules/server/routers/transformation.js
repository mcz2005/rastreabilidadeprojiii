const express = require('express');
const router = express.Router();
const Transformation = require('../controllers/transformation');
const passport = require('passport');
require('../config/passport')(passport);


router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Transformation.getAll(req, res)
});
router.get('/getbyid/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Transformation.getById(req, res)
});
router.get('/getbyvaluechainoperatorid', passport.authenticate('jwt', {session: false}), function (req, res) {
    Transformation.getByValueChainOperator(req, res)
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    Transformation.create(req, res)
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Transformation.update(req, res)
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    Transformation.delete(req, res)
});

module.exports = router;