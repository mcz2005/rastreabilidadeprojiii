const express = require('express');
const router = express.Router();
const TypeOfHarvest = require('../controllers/typeOfHarvest');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfHarvest.getAll(req, res);
});
router.get('/getById/:typeOfHarvestId', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfHarvest.getById(req, res);
});
router.get('/available', function (req, res) {
    TypeOfHarvest.getAvailable(req, res);
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfHarvest.create(req, res);
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfHarvest.update(req, res);
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfHarvest.updateAvailability(req, res);
});

module.exports = router;