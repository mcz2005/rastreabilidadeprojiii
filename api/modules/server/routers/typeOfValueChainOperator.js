const express = require('express');
const router = express.Router();
const TypeOfValueChainOperator = require('../controllers/typeOfValueChainOperator');
const passport = require('passport');

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfValueChainOperator.getAll(req, res);
});
router.get('/available/', function (req, res) {
    TypeOfValueChainOperator.getAvailable(req, res);
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfValueChainOperator.create(req, res);
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfValueChainOperator.update(req, res);
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    TypeOfValueChainOperator.updateAvailability(req, res);
});

module.exports = router;
