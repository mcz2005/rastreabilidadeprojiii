const express = require('express');
const router = express.Router();
const User = require('../controllers/user');
const passport = require('passport');

require('../config/passport')(passport);

router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    User.getAll(req, res)
});
router.get('/available', passport.authenticate('jwt', {session: false}), function (req, res) {
    User.getAvailable(req, res);
});
router.get('/allusersallroles', passport.authenticate('jwt', {session: false}), function (req, res) {
    User.getRolesFromAllUsers(req, res);
});
router.get('/getbyid/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    User.getById(req, res)
});
router.get('/getByValueChainOperator', passport.authenticate('jwt', {session: false}), function (req, res) {
    User.getByValueChainOperatorId(req, res)
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    User.register(req, res)
});
router.post('/login', function (req, res) {
    User.login(req, res)
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    User.update(req, res)
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    User.delete(req, res)
});

module.exports = router;