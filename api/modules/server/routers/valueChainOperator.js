const express = require('express');
const router = express.Router();
const ValueChainOperator = require('../controllers/valueChainOperator');
const passport = require('passport');


router.get('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ValueChainOperator.getAll(req, res);
});
router.get('/available', passport.authenticate('jwt', {session: false}), function (req, res) {
    ValueChainOperator.getAvailable(req, res);
});
router.get('/:id', function (req, res) {
    ValueChainOperator.getById(req, res);
});
router.post('/', passport.authenticate('jwt', {session: false}), function (req, res) {
    ValueChainOperator.create(req, res);
});
router.put('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ValueChainOperator.update(req, res);
});
router.delete('/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    ValueChainOperator.updateAvailability(req, res);
});

module.exports = router;
